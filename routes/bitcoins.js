var bitcoin = require('../lib/bitcoin')
var waterfall = require('async-waterfall')
var express = require('express');
var deasync = require('deasync');
var jwtverify = require('./jwtverify');
var router = express.Router();

var btckey = require('../lib/btckey')

var config = require('config')

var dsController = require('../lib/dataServerController')

var bitcoinAccountName = config.get('bitcoin.accountName')



function adminAuthPlus(permissionSet) {
  if (Array.isArray(permissionSet)) {
    permissionSet.unshift(["full-admin"], ["btc-admin"])
  }
  else {
    if (permissionSet) {
      permissionSet = [["full-admin"], ["btc-admin"], permissionSet]
    }
    else {
      permissionSet = [["full-admin"], ["btc-admin"]]
    }
  }
  return permissionSet;
}

router.use(function (req, res, next) {
  console.log("Made it to bitcoins route");
  next();
})

router.get('/blockcount/', function (req, res, next) {
  bitcoin.getBlockCount(function (error, result) {
    res.json({ error, result })
  })
});

router.get('/blockwalk/:walkcount', function (req, res, next) {
  bitcoin.getBestBlockHash(function (error, result) {
    console.log("FirstBlock", result);
    bitcoin.getBlock(result, function (error, result) {
      var count = req.query.walkcount;
      function previousBlockHash(count, result) {
        console.log("Next Block", result.previousblockhash)
        if (count > 0) {
          bitcoin.getBlock(result.previousblockhash, function (error, result) {
            previousBlockHash(count - 1, result);
          })
        }
        else {
          result.tx = result.tx.map(function (txId) {
            var ret = void 0
            bitcoin.decodeRawTransaction(txId, function (error, result) {
              ret = result;
            })
            while (ret === undefined) {
              deasync.sleep(100);
            }
            return ret;
          });
          res.json({ error, result })
        }
      }
      previousBlockHash(count - 1, result)
    })
  })
})

router.get('/decode/*', function (req, res, next) {
  var txHex = req.url.split('/').pop()
  txHex = txHex.split("?")[0]
  console.log("decode:", txHex)
  bitcoin.decodeRawTransaction(txHex, function (error, response) {
    res.json({ error, result: response })
  })
})

router.get('/transactionfee/:txid', jwtverify.checkAuth(adminAuthPlus([["btcTransactionFee"]])), function (req, res, next) {
  console.log("transactionfee", req.params)
  bitcoin.getTransactionFee(req.params.txid, function (error, result) {
    res.json({ error, result })
  })
})

router.get('/unspent', jwtverify.checkAuth(adminAuthPlus([["btc-transaction"]])), function (req, res, next) {
  bitcoin.unspent(function (error, result) {
    res.json({ error, result })
  })
});

router.get('/sinceblock/:blkid*?', jwtverify.checkAuth(adminAuthPlus([["btcSinceBlock"]])), function (req, res, next) {
  if (req.query.blkid) {
    // bitcoin-cli listsinceblock ( "blockhash" target_confirmations include_watchonly)
    bitcoin.listSinceBlock(req.query.blkid, function (error, result) {
      res.json({ error, result })
    })
  }
  else {
    // bitcoin-cli listsinceblock ( "blockhash" target_confirmations include_watchonly)
    bitcoin.listSinceBlock(function (error, result) {
      res.json({ error, result })
    })
  }
});

router.get('/receivedbyaccounts', jwtverify.checkAuth(adminAuthPlus()), function (req, res, next) {
  // bitcoin-cli listreceivedbyaccount ( minconf include_empty include_watchonly)
  bitcoin.listReceivedByAccount(function (error, result) {
    res.json({ error, result });
  })
})

router.get('/receivedbyaddresses', jwtverify.checkAuth(adminAuthPlus([["btcGetWalletAddresses"]])), function (req, res, next) {
  // bitcoin-cli listreceivedbyaddress ( minconf include_empty include_watchonly)
  bitcoin.listReceivedByAddress(0, true, function (error, result) {
    res.json({ error, result });
  })
})

router.get('/transaction/:txid', jwtverify.checkAuth(adminAuthPlus([["btcGetTransaction"]])), function (req, res, next) {
  // bitcoin-cli getrawtransaction "txid" ( verbose )
  bitcoin.getRawTransaction(req.params.txid, 1, function (error, result) {
    res.json({ error, result })
  })
})

router.get('/transaction', jwtverify.checkAuth(adminAuthPlus([["btcGetTransaction"]])), function (req, res, next) {
  bitcoin.getRawTransaction(req.body.txid, 1, function (error, result) {
    if (error) {
      console.log("/bitcoins - getRawTransaction:Error", error)
    }
    else {


      var method = "post"
      var url = "/bitcoin/server/addTransaction"
      var postData = result
      var authBody = {
        server: "dataServer",
        permissions: {
          btcAddTransaction: true
        }
      }

      var token = jwtverify.getKeyFromCert(btckey.cert, authBody, { expiresIn: 3000 })

      console.log("TODO: btcGetTransaction:token", token)

      dsController.callApi(method, url, postData, token, function (error, result) {
        if (error) console.log("TODO: btcGetTransaction:callback:Error", error)
        console.log("TODO: btcGetTransaction:callback:Result", result)
        res.send(result)

      })
    }

  })
})

router.post('/wallet', jwtverify.checkAuth(adminAuthPlus([["btcCreateWallet"]])), function (req, res, next) {
  // bitcoin-cli getnewaddress ( "account" )
  bitcoin.getNewAddress(bitcoinAccountName, function (error, result) {
    res.json({ error, result })
  })
})


// == Blockchain ==
// bitcoin-cli getbestblockhash
// bitcoin-cli getblock "blockhash" ( verbose )
// bitcoin-cli getblockchaininfo
// bitcoin-cli getblockcount
// bitcoin-cli getblockhash height
// bitcoin-cli getblockheader "hash" ( verbose )
// bitcoin-cli getchaintips
// bitcoin-cli getdifficulty
// bitcoin-cli getmempoolancestors txid (verbose)
// bitcoin-cli getmempooldescendants txid (verbose)
// bitcoin-cli getmempoolentry txid
// bitcoin-cli getmempoolinfo
// bitcoin-cli getrawmempool ( verbose )
// bitcoin-cli gettxout "txid" n ( include_mempool )
// bitcoin-cli gettxoutproof ["txid",...] ( blockhash )
// bitcoin-cli gettxoutsetinfo
// bitcoin-cli preciousblock "blockhash"
// bitcoin-cli pruneblockchain
// bitcoin-cli verifychain ( checklevel nblocks )
// bitcoin-cli verifytxoutproof "proof"

// == Control ==
// bitcoin-cli getinfo
// bitcoin-cli getmemoryinfo
// bitcoin-cli help ( "command" )
// bitcoin-cli stop

// == Generating ==
// bitcoin-cli generate nblocks ( maxtries )
// bitcoin-cli generatetoaddress nblocks address (maxtries)

// == Mining ==
// bitcoin-cli getblocktemplate ( TemplateRequest )
// bitcoin-cli getmininginfo
// bitcoin-cli getnetworkhashps ( nblocks height )
// bitcoin-cli prioritisetransaction <txid> <priority delta> <fee delta>
// bitcoin-cli submitblock "hexdata" ( "jsonparametersobject" )

// == Network ==
// bitcoin-cli addnode "node" "add|remove|onetry"
// bitcoin-cli clearbanned
// bitcoin-cli disconnectnode "address" 
// bitcoin-cli getaddednodeinfo ( "node" )
// bitcoin-cli getconnectioncount
// bitcoin-cli getnettotals
// bitcoin-cli getnetworkinfo
// bitcoin-cli getpeerinfo
// bitcoin-cli listbanned
// bitcoin-cli ping
// bitcoin-cli setban "subnet" "add|remove" (bantime) (absolute)
// bitcoin-cli setnetworkactive true|false

// == Rawtransactions ==
// bitcoin-cli createrawtransaction [{"txid":"id","vout":n},...] {"address":amount,"data":"hex",...} ( locktime )
// bitcoin-cli decoderawtransaction "hexstring"
// bitcoin-cli decodescript "hexstring"
// bitcoin-cli fundrawtransaction "hexstring" ( options )
// bitcoin-cli getrawtransaction "txid" ( verbose )
// bitcoin-cli sendrawtransaction "hexstring" ( allowhighfees )
// bitcoin-cli signrawtransaction "hexstring" ( [{"txid":"id","vout":n,"scriptPubKey":"hex","redeemScript":"hex"},...] ["privatekey1",...] sighashtype )

// == Util ==
// bitcoin-cli createmultisig nrequired ["key",...]
// bitcoin-cli estimatefee nblocks
// bitcoin-cli estimatepriority nblocks
// bitcoin-cli estimatesmartfee nblocks
// bitcoin-cli estimatesmartpriority nblocks
// bitcoin-cli signmessagewithprivkey "privkey" "message"
// bitcoin-cli validateaddress "address"
// bitcoin-cli verifymessage "address" "signature" "message"

// == Wallet ==
// bitcoin-cli abandontransaction "txid"
// bitcoin-cli addmultisigaddress nrequired ["key",...] ( "account" )
// bitcoin-cli addwitnessaddress "address"
// bitcoin-cli backupwallet "destination"
// bitcoin-cli bumpfee "txid" ( options ) 
// bitcoin-cli dumpprivkey "address"
// bitcoin-cli dumpwallet "filename"
// bitcoin-cli encryptwallet "passphrase"
// bitcoin-cli getaccount "address"
// bitcoin-cli getaccountaddress "account"
// bitcoin-cli getaddressesbyaccount "account"
// bitcoin-cli getbalance ( "account" minconf include_watchonly )
// bitcoin-cli getnewaddress ( "account" )
// bitcoin-cli getrawchangeaddress
// bitcoin-cli getreceivedbyaccount "account" ( minconf )
// bitcoin-cli getreceivedbyaddress "address" ( minconf )
// bitcoin-cli gettransaction "txid" ( include_watchonly )
// bitcoin-cli getunconfirmedbalance
// bitcoin-cli getwalletinfo
// bitcoin-cli importaddress "address" ( "label" rescan p2sh )
// bitcoin-cli importmulti "requests" "options"
// bitcoin-cli importprivkey "bitcoinprivkey" ( "label" ) ( rescan )
// bitcoin-cli importprunedfunds
// bitcoin-cli importpubkey "pubkey" ( "label" rescan )
// bitcoin-cli importwallet "filename"
// bitcoin-cli keypoolrefill ( newsize )
// bitcoin-cli listaccounts ( minconf include_watchonly)
// bitcoin-cli listaddressgroupings
// bitcoin-cli listlockunspent
// bitcoin-cli listreceivedbyaccount ( minconf include_empty include_watchonly)
// bitcoin-cli listreceivedbyaddress ( minconf include_empty include_watchonly)
// bitcoin-cli listsinceblock ( "blockhash" target_confirmations include_watchonly)
// bitcoin-cli listtransactions ( "account" count skip include_watchonly)
// bitcoin-cli listunspent ( minconf maxconf  ["addresses",...] [include_unsafe] )
// bitcoin-cli lockunspent unlock ([{"txid":"txid","vout":n},...])
// bitcoin-cli move "fromaccount" "toaccount" amount ( minconf "comment" )
// bitcoin-cli removeprunedfunds "txid"
// bitcoin-cli sendfrom "fromaccount" "toaddress" amount ( minconf "comment" "comment_to" )
// bitcoin-cli sendmany "fromaccount" {"address":amount,...} ( minconf "comment" ["address",...] )
// bitcoin-cli sendtoaddress "address" amount ( "comment" "comment_to" subtractfeefromamount )
// bitcoin-cli setaccount "address" "account"
// bitcoin-cli settxfee amount
// bitcoin-cli signmessage "address" "message"




module.exports = router;


