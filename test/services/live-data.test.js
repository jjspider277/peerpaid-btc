const assert = require('assert');
const app = require('../../src/app');

describe('\'live-data\' service', () => {
  it('registered the service', () => {
    const service = app.service('live-data');

    assert.ok(service, 'Registered the service');
  });
});
