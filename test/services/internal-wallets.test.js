const assert = require('assert');
const app = require('../../src/app');

describe('\'internal-wallets\' service', () => {
  it('registered the service', () => {
    const service = app.service('internal-wallets');

    assert.ok(service, 'Registered the service');
  });
});
