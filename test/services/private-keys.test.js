const assert = require('assert');
const app = require('../../src/app');

describe('\'privateKeys\' service', () => {
  it('registered the service', () => {
    const service = app.service('private-keys');

    assert.ok(service, 'Registered the service');
  });
});
