const assert = require('assert');
const app = require('../../src/app');

describe('\'internal-ledger\' service', () => {
  it('registered the service', () => {
    const service = app.service('internal-ledger');

    assert.ok(service, 'Registered the service');
  });
});
