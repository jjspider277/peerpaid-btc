const assert = require('assert');
const app = require('../../src/app');

describe('\'bitcoinTransaction_proccessed\' service', () => {
  it('registered the service', () => {
    const service = app.service('bitcoin-transaction-proccessed');

    assert.ok(service, 'Registered the service');
  });
});
