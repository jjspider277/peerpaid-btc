#!/bin/bash

rsync -azv --exclude 'node_modules' --exclude '.git' --delete --delete-excluded ./ thelazycoder@peerpaid-dev-btc:~/peerpaid-btc/

ssh thelazycoder@peerpaid-dev-btc
