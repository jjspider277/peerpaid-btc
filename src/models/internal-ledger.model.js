// internal-ledger-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const internalLedger = new Schema({
    walletTo: { type: String, required: true },
    amount: { type: Number, required: true },
    currency: { 
      type: String,
      enum: [
        'BTC',
      ], required: true
    },
    transaction: { type: Schema.ObjectId, required: true },
    status: {
      type: String,
      enum: [
        'PENDING',
        'PROCESSED'
      ], required: true
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
  });

  return mongooseClient.model('internal-ledger', internalLedger);
};
