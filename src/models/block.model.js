var mongoose = require('mongoose');

module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const bitcoinTransactions = new Schema({
    // Begin Raw Transaction
    hash: { type: String, required: true },
    size: { type: Number, required: true },
    height: { type: Number, required: true },
    version: { type: Number, required: true },
    merkleroot: { type: String, required: true },
    tx: [
      { type: String, require: false }
    ],
    txProcessed: [
      { type: String, require: false }
    ],
    time: { type: String, required: true },
    nonce: { type: String, required: false },
    confirmations:  { type: Number, required: true },
    nextblockhash:  { type: String, required: false },
    

    //End Raw Transaction
    createdAt: { type: Date, default: Date.now, index: true },
    updatedAt: { type: Date, default: Date.now, index: true }
  });
  return mongooseClient.model('block', bitcoinTransactions);
};




