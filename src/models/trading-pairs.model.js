module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const tradingPairs = new Schema({
    base_decimals: { type: Number, required: true },
    minimum_order: { type: String, required: false },
    name:{type: String, required: true, unique: true},
    counter_decimals:{type: Number, required: true},
    trading_enabled: { type: String },
    url_symbol: { type: String, unique: true },
    description: {type: String}
  });

  return mongooseClient.model('trading-pairs', tradingPairs);
};

