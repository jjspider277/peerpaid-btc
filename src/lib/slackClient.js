var Slack = require('slack-node');

module.exports = (app, data) => {
  return new Promise((resolve, reject) => {
    slack = new Slack();
    slack.setWebhook(app.get('slackIntegration').webhookUrl);
    if (data.message ) {
      slack.webhook({
        channel: app.get('slackIntegration').channel,
        username: "Auditor",
        text: data.message
      }, function (err, response) {
        console.log(response);
        return resolve();
      });
    } else {
      console.log("Data doesnt have a valid message");
      return resolve();
    }

  })
}
