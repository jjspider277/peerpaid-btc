// Initializes the `internal-wallets` service on path `/internal-wallets`
const createService = require('feathers-mongoose');
const createModel = require('../../models/internal-wallets.model');
const hooks = require('./internal-wallets.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'internal-wallets',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/internal-wallets', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('internal-wallets');

  service.hooks(hooks);
};
