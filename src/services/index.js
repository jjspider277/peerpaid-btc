const users = require('./users/users.service.js');
const bitcoinTransactions = require('./bitcoin-transactions/bitcoin-transactions.service.js');
const wallets = require('./wallets/wallets.service.js');
const requests = require('./requests/requests.service.js');
const currencyAccounts = require('./currency-accounts/currency-accounts.service.js');
const tradingPairs = require('./trading-pairs/trading-pairs.service.js');
const internalLedger = require('./internal-ledger/internal-ledger.service.js');

const internalTransaction = require('./internal-transaction/internal-transaction.service.js');
const liveData = require('./live-data/live-data.service.js');

const blockService = require('./blocks/blocks.service.js');


const privateKeys = require('./private-keys/private-keys.service.js');


const internalWallets = require('./internal-wallets/internal-wallets.service.js');


module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(users);
  app.configure(bitcoinTransactions);
  app.configure(wallets);
  app.configure(requests);
  app.configure(currencyAccounts);
  app.configure(tradingPairs);
  app.configure(internalLedger);
  app.configure(internalTransaction);
  app.configure(liveData);
  app.configure(blockService)
  app.configure(privateKeys);
  app.configure(internalWallets);
};
