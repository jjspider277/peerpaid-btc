// Initializes the `privateKeys` service on path `/private-keys`
const createService = require('feathers-mongoose');
const createModel = require('../../models/private-keys.model');
const hooks = require('./private-keys.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'private-keys',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/private-keys', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('private-keys');

  service.hooks(hooks);
};
