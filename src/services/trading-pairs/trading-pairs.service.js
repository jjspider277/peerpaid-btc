// Initializes the `trading-pairs` service on path `/trading-pairs`
const createService = require('feathers-mongoose');
const createModel = require('../../models/trading-pairs.model');
const hooks = require('./trading-pairs.hooks');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'trading-pairs',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/trading-pairs', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('trading-pairs');

  service.hooks(hooks);
};
