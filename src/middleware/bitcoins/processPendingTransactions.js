
var Decimal = require('decimal.js');
var Promise = require('bluebird');

const getFirstItem = require('../../lib/common').getFirstItem;

module.exports = async (app, definedSkip, definedLimit) => {
  const minConfirmations = app.get('bitcoin').minConfirmations;
  if (isNaN(minConfirmations) || minConfirmations <= 0) {
    console.log('Minimum Confirmations is an invalid number');
    return;
  }

  var skip = definedSkip;
  var limit = definedLimit;
  var modifiedTransactions = [];
  function processTransactions(skip, limit) {
    return app.service('internal-transaction').find({
      query: {
        _id: { $nin: modifiedTransactions },
        $limit: limit,
        $skip: skip,
        status: 'PENDING',
        currency: 'BTC',
        type: 'RECEIVE_BITCOINS',
        currencyAccountDestination: { $exists: false },
      }
    }).then(pendingTransactions => {
      var total = pendingTransactions.total;
      var data = pendingTransactions.data;

      Promise.map(data, function (tx) {
        const confirmations = new Decimal(tx.details.confirmations).add(1).toNumber();
        return app.service('internal-transaction').update(tx._id, {
          $set: {
            'details.confirmations': confirmations
          }
        }).then(transactionUpdated => {
          modifiedTransactions = modifiedTransactions.concat(transactionUpdated._id);
          //console.log('Modified transaction ', modifiedTransactions);
          return transactionUpdated;
        }).catch(error => {
          return transactionUpdated;
        });
      }, { concurrency: limit }).then(chunkUpdated => {
        if (data.length > 0) {
          return processTransactions(skip, limit);
        } else {
          return;
        }
      });
    }).then(allBlocksUpdated => {
      return true;
    });
  }
  return new Promise((resolve, reject) => {
    return resolve(processTransactions(skip, limit));
  });
};