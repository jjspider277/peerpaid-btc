const bitcoin = require('../../lib/bitcoin');

// var dataServer = require('../../lib/feathersDataServerClient');

const debug = require('../../lib/debug');
const main = require('./main');
const processingBlocks = require('./processingBlocks');
const checkInternalWallets = require('./checkInternalWallets');
const consolidateWallet = require('./consolidateWallet')
const sendTransactionsChunk = require('./sendTransactionChunk')
const allocateInternalWallets = require('./allocateInternalWallets')
//const unspentTo = require('./unspentTXOS');

module.exports = function (app) {
  function init(app) {
    module.exports.requestService = app.service('requests');
    module.exports.walletService = app.service('wallets');
    module.exports.transactionService = app.service('bitcoin-transactions');
    module.exports.currencyAccountService = app.service('currency-accounts');
    module.exports.walletService = app.service('wallets');

    // check if I have the minimum internal wallets
    if (!module.exports.walletService) {
      setTimeout(() => {
        init(app);
      }, 100);
    } else {
      // unspentTo(app,'mur7BhrYskhoJahPQ8FoYqzJDgqaG6nqa2').then(result =>{
      //   console.log(result);
      // consolidateWallet(app).catch(error => {
      //   console.log('index.js:40 ', error.message);
      //   return;
      // });
      // return;
      // })
      checkInternalWallets(app).then(internalWalletsGood => {
        const bitcoinConfiguration = app.get('bitcoin');
        if (isNaN(bitcoinConfiguration.sendChunkEachSeconds) || bitcoinConfiguration.sendChunkEachSeconds <= 1000) {
          slackClient(app, { message: 'sendChunkEachSeconds is an invalid number.' });
          return reject('sendChunkEachSeconds is an invalid number. ');
        } else {
          setInterval((() => {
            sendTransactionsChunk(app).catch(error => {
              console.log('index.js:34 ', error.message);
            });
          }), app.get('bitcoin').sendChunkEachSeconds);
        }
        processingBlocks(app);
      });

    }
  }

  setTimeout(() => {
    init(app);
  }, 100);
};
