var debug = require('../../lib/debug');
const getFirstItem = require('../../lib/common').getFirstItem;
const Decimal = require('decimal.js');
const bitcore = require('bitcore-lib');
const Promise = require('bluebird');

const unspentTXOS = require('./unspentTXOS')
const slackClient = require('../../lib/slackClient');
const Insight = require('./insight');

debug = () => { };

module.exports = (app) => {

  var internalTransactionService = app.service('internal-transaction');
  var internalWallets = app.service('internal-wallets');
  var privateKeyService = app.service('private-keys');
  var currencyAccountService = app.service('currency-accounts');

  var insightServer = app.get('insightServer');
  var insight = new Insight(insightServer.url, insightServer.room);

  // We need to define a parameter in config to define a fixed time to consolidate wallets.
  var daysAgo = app.get('daysAgo') || 2;
  var bitcoinConfig = app.get('bitcoin') || false;
  var minConfirmations = bitcoinConfig.minConfirmations;
  return new Promise((resolve, reject) => {
    let dateQuery = new Date();
    dateQuery.setDate(dateQuery.getDate() - daysAgo);
    return internalTransactionService.find({
      paginate: false,
      query: {
        currencyAccountOrigin: { $exists: false },
        currencyAccountDestination: { $exists: false },
        updatedAt: { $gte: dateQuery },
        status: 'PENDING',
        'details.confirmations': { $gte: minConfirmations },
        type: 'RECEIVE_BITCOINS'
      },
    }).then(transactions => {
      console.log('Consolidate Wallet', transactions);
      return transactions.map(function (transaction) {
        return transaction.walletDestination;
      });
    }).then(walletsDuplicated => {
      let wallets = new Set(walletsDuplicated);
      return wallets;
      // return walletsDuplicated.filter(wallet => {
      //   if (wallets[wallet] != void 0) {
      //     return false;
      //   } else {
      //     wallets[wallet] = true;
      //     return true;
      //   }
      // });
    }).then(wallets => {
      let unspentUTXOsArray = [];
      let privateKeysArray = [];
      Promise.map(wallets, function (wallet) {
        return privateKeyService.find({
          query: {
            address: wallet
          }
        }).then(walletsFound => {
          //Fetching the information need it for create the private key array
          let walletFound = getFirstItem(walletsFound);
          if (walletFound == void 0) {
            console.log('consolidateWallet.js: 68  No Wallet found !!');
            return reject(new Error('No wallet found '));
          }
          privateKeysArray = [].concat(privateKeysArray, [walletFound.privateKey]);
          return unspentTXOS(app, wallet, minConfirmations).catch(error => {
            // We dont need to check this error, probably because we already processed this entry
            //console.log(error.message)
            return reject(error);
          });
        }).catch(error => {
          console.log('consolidateWallet.js: 78', JSON.stringify(error));
          return reject(error);
        });
      }, { concurrency: 50 }).then(unspentUTXOs => {
        unspentUTXOsArray = [].concat(unspentUTXOs);
        return { pk: privateKeysArray, utxos: unspentUTXOsArray };
      }).then(data => {

        var utxos = [];
        data.utxos.forEach(element => {
          if (element == void 0) {
            //
          } else {
            utxos = [].concat(utxos, element);
          }
        });
        data.utxos = utxos;
        if (data.utxos.length == 0) {
          console.log('consolidateWallet.js:96 No utxos found');
          return reject(new Error('No unspent transactions found'));
        }
        return internalWallets.find({
          query: {
            $sort: {
              updatedAt: 1
            }
          }
        }).then(internalWalletsFound => {
          var internalWallet = getFirstItem(internalWalletsFound);
          data.internalWallet = internalWallet;
          return data;
        }).then(data => {
          console.log('consolidateWallet.js:110 internal wallet selected: ', data.internalWallet.address);
          return unspentTXOS(app, data.internalWallet.address, minConfirmations).then(internalWalletUTXOs => {
            console.log('consolidateWallet.js:112 Unspent transactions found!! ', internalWalletUTXOs);
            if (internalWalletUTXOs.length == 0) {
              slackClient(app, { message: 'The internal wallet: ' + data.internalWallet.address + 'doesn\'t have funds ' });
            }
            data.utxos = data.utxos.concat(internalWalletUTXOs);
            data.pk = data.pk.concat(data.internalWallet.privateKey);

            //Cleaning data utxos for undefined, 0 and null values
            data.utxos = data.utxos.filter(function (e) { return e });
            console.log('Data after the clean up: ', data);
            return data;
          }).catch(error => {
            console.log('consolidateWallet.js:124 ', error.stack);
            return reject('No unspent transactions found');
          });
        });
      }).catch(error => {
        console.log('consolidateWallet.js: 129', JSON.stringify(error));
        return reject(error);
      }).then(data => {
        if (data == void 0) {
          return reject();
        }

        //all good, we should have at this point the array for unspent tx,
        // the private keys array and the internal wallet created;
        //console.log('Private Keys       ', data.pk);
        //check if the utxos are only for the internal wallet..

        if (data === void 0 || data.utxos.length == 0 || !data.internalWallet) {
          console.log('consolidateWallet.js:142 No transactions to spend');
          return resolve();
        } else {
          console.log('consolidateWallet.js:145 Selected wallet ', data.internalWallet.address);
          var amount = data.utxos.reduce((accumulator, value) => {
            if (value == void 0) {
              return accumulator;
            } else {
              var decimal = new Decimal(value.satoshis);
              return accumulator.add(decimal);
            }
          }, new Decimal(0.0));
          data.amount = amount.toNumber();

          if (data.amount <= bitcoinConfig.minimumSatoshisForConsolidate) {
            slackClient(app, { message: 'Not enough satoshis to spend consolidating a transaction: ' + JSON.stringify({ amount: data.amount, minimumDefined: bitcoinConfig.minimumSatoshisForConsolidate }, null, '\t') });
            return resolve();
          } else {

            let tx = new bitcore.Transaction();
            tx.from(data.utxos);
            tx.change(data.internalWallet.address);
            tx.sign(data.pk);
            if (tx.isFullySigned()) {
              console.log('The consolidating transaction was fully signed');
              tx.serialize();
            } else {
              console.log('The transaction is not fully signed');
              return resolve();
            }
            if (data.utxos.length === 1) {
              //Internal wallet are not the only sender 
              return resolve();
            }
            if (data.amount <= tx.getFee()) {
              // We need to define some logic here with fees 
              slackClient(app, { message: 'The fee is higher than the value: ' + JSON.stringify({ amount: data.amount, fee: tx.getFee() }, null, '\t') });
              return resolve();
            }
            return new Promise((resolve, reject) => {
              insight.broadcast(tx, (err, transactionId) => {
                if (err) {
                  //console.log('Error Broadcasting:', err);
                  slackClient(app, { message: 'Error when broadcasting a transaction: ' + JSON.stringify(err, null, '\t') });
                  return reject(err);
                }
                else {
                  console.log('consolidateWallet.js:189 blockChain Transaction hash created :', transactionId);
                  // slackClient(app, { message: 'Transactions consolidated: ' + JSON.stringify(tx.hash, null, '\t') });
                  return resolve({ amount: amount, transaction: transactionId });
                }
              });
            }).then(info => {
              console.log('consolidateWallet.js:195 The incoming transactions was succesfully stored into an internal wallet');

              var message = {
                address: data.internalWallet.address,
                amount: info.amount,
                transaction: info.transaction,
                utxos: data
              };
              slackClient(app, { message: 'A new consolidated transaction has been sent to an internal wallet: ' + JSON.stringify(message, null, '\t') });
              return internalWallets.update(data.internalWallet._id, {
                $set: {
                  updatedAt: new Date()
                }
              }).then(internalWalletUpdated => {
                Promise.map(data.utxos, function (utxo) {
                  // console.log('Updating utxo', utxo);
                  var tx = JSON.stringify(utxo);
                  tx = JSON.parse(tx);
                  //console.log('TX within consolidate wallet', tx);
                  return internalTransactionService.find({
                    query: {
                      blockchainTransaction: tx.txid
                    }
                  }).then(internalTransactionsFound => {
                    //console.log("internalTransactionsFound  ", internalTransactionsFound);
                    const internalTx = getFirstItem(internalTransactionsFound);
                    if (internalTx == void 0) {
                      return resolve();
                    } else {
                      return internalTransactionService.update(internalTx._id, {
                        $set: {
                          status: 'SUCCESS'
                        }
                      }).then(transactionUpdated => {
                        //console.log('consolidatedWallet.js:43  ----> Updated at put to success  ', transactionUpdated);
                        return currencyAccountService.find({
                          query: {
                            accountId: transactionUpdated.walletDestination,
                            currencyType: 'BTC'
                          }
                        }).then(currencyAccounts => {
                          let currencyAccount = getFirstItem(currencyAccounts);
                          return currencyAccountService.update(currencyAccount._id, {
                            $addToSet: {
                              txids: transactionUpdated._id
                            },
                            $pull: {
                              txidsTentative: transactionUpdated._id
                            },
                            $inc: { tentativeAmount: new Decimal(transactionUpdated.amount).negated(), amount: new Decimal(transactionUpdated.amount) }
                          }).then(currencyAccountUpdated => {
                            return resolve(transactionUpdated);
                          });
                        });
                      });
                    }
                  });
                });
              });
            }).catch(error => {
              return error;
            });
          }
        }
      });
    });
  });
};