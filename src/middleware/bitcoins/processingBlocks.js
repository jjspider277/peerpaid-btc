var request = require('request');

var bitcoreLib = require('bitcore-lib');
var Decimal = require('decimal.js');
var io = require('socket.io-client');
var Insight = require('./Insight');
var axios = require('axios');

var processTransaction = require('./processTransaction');
var processPendingTransactions = require('./processPendingTransactions');

var Promise = require('bluebird');
const getFirstItem = require('../../lib/common').getFirstItem;

const consolidateWallet = require('./consolidateWallet');

module.exports = async (app) => {

  const minConfirmations = app.get('bitcoin').minConfirmations;
  if (isNaN(minConfirmations) || minConfirmations <= 0) {
    console.log('Minimum Confirmations is an invalid number');
    return;
  }

  var insightServer = app.get('insightServer');
  var insight = new Insight(insightServer.url, insightServer.room);

  var socket = io(insightServer.url);
  socket.on('connect', function () {
    // Join the room.
    console.log('Subscribe to', 'inv');
    socket.emit('subscribe', 'inv');
    console.log('Subscribe to', 'sync');
    socket.emit('subscribe', 'sync');
  });

  socket.on('status', function (data) {
    console.log('status', data);
  });

  socket.on('block', function (block) {
    console.log('processingBlock:41 New block Received:  ', block);
    var skip = 0;
    var limit = 1;

    return processPendingTransactions(app,skip, limit).then(allTransactionUpdated => {
      console.log('All Block updated ');
      try {
        var lastBlock = axios.get(insightServer.url + '/insight-api/block/' + block, {})
          .then(blockFetched => {
            return blockFetched.data;
          })
          .then(function (response) {
            var txArray = response.tx;

            var promisesProcessTransaction = [];

            Promise.map(txArray, function (tx) {
              return axios.get(insightServer.url + '/insight-api/tx/' + tx, {})
                .then(response => {
                  var vout = response.data.vout;
                  var parentTx = response.data;
                  if (vout == undefined) {
                    return;
                  } else {
                    return parentTx;

                    // promisesProcessTransaction = txArray.map(tx => {
                    //   console.log("transaction tx 1375", tx)
                    //   return new Promise((resolve, reject) => {
                    //     vout.forEach(trx => {
                    //       return processTransaction(app, trx, parentTx).then(solvedPromise => {
                    //         return solvedPromise
                    //       })
                    //     })
                    //   })
                    // })
                  }
                });
            }, { concurrency: 50 }).then(transactions => {

              return new Promise((resolve, reject) => {

                Promise.map(transactions, function (transaction) {
                  Promise.map(transaction.vout, function (trx) {
                    // console.log("trx",trx)
                    return new Promise((resolve, reject) => {
                      return processTransaction(app, trx, transaction).then(processTransactionResult => {
                        //console.log("pfsdfsdfsd",transaction)
                        return resolve(trx);
                      }).catch(error => {
                        //console.log("Error", error);
                        return resolve(error);
                      });
                    });
                  });
                }, { concurrency: 50 }).then(results => {
                  //  console.log("asdasd",results)
                  return resolve(results);
                });
              });
            });
          }).then(getLast => {
            return app.service('blocks').find({
              query: {
                //hash: blockDetails.hash
              }
            }).then(blockFetched => {
              var newBlock = getFirstItem(blockFetched);
              if (newBlock === void 0) {
                return app.service('blocks').create({
                  hash: blockDetails.hash,
                  size: blockDetails.size,
                  height: blockDetails.height,
                  time: blockDetails.time,
                  version: blockDetails.version,
                  merkleroot: blockDetails.merkleroot,
                  tx: blockDetails.tx,
                  confirmations: blockDetails.confirmations,
                  nextblockhash: blockDetails.nextblockhash
                }).then(blockCreated => {
                  block = blockCreated.nextblockhash;
                  return block;
                });
              } else {
                return app.service('blocks').update(newBlock._id, {
                  $set: {
                    hash: blockDetails.hash,
                    size: blockDetails.size,
                    height: blockDetails.height,
                    time: blockDetails.time,
                    version: blockDetails.version,
                    merkleroot: blockDetails.merkleroot,
                    tx: blockDetails.tx,
                    confirmations: blockDetails.confirmations,
                    nextblockhash: blockDetails.nextblockhash
                  }
                }).then(blockUpdated => {
                  console.log('new hash   ', blockUpdated.hash);
                  block = blockUpdated.nextblockhash;
                  return block;
                });
              }
            });
          });
      } catch (error) {
        console.log('Error within the loop', error);
      }
    }).then(walletsConsolidate => {
      return consolidateWallet(app).catch(error => {
        console.log('processingBlocks.js:221 ', error.message);
        return;
      });
    });
  });




  async function processingLatestBlock(block) {
    while (block != null) {
      try {
        var processing = await axios.get(insightServer.url + '/insight-api/block/' + block, {})
          .then(blockFetched => {
            blockDetails = blockFetched.data;
            return blockDetails;
          })
          .then(function (response) {
            var txArray = response.tx;
            var promisesProcessTransaction = [];

            Promise.map(txArray, function (tx) {
              return axios.get(insightServer.url + '/insight-api/tx/' + tx, {})
                .then(response => {
                  var vout = response.data.vout;
                  var parentTx = response.data;
                  if (vout == undefined) {
                    return;
                  } else {
                    return parentTx;

                    // promisesProcessTransaction = txArray.map(tx => {
                    //   console.log('transaction tx 1375', tx);
                    //   return new Promise((resolve, reject) => {
                    //     vout.forEach(trx => {
                    //       return processTransaction(app, trx, parentTx).then(solvedPromise => {
                    //         return solvedPromise;
                    //       });
                    //     });
                    //   });
                    // });
                  }
                });
            }, { concurrency: 50 }).then(transactions => {
              return new Promise((resolve, reject) => {
                Promise.map(transactions, function (transaction) {
                  Promise.map(transaction.vout, function (trx) {
                    // console.log("trx",trx)
                    return new Promise((resolve, reject) => {
                      return processTransaction(app, trx, transaction).then(processTransactionResult => {
                        //console.log("pfsdfsdfsd",transaction)
                        return resolve(trx);
                      }).catch(error => {
                        //console.log("Error", error);
                        return resolve(error);
                      });
                    });
                  });
                }, { concurrency: 50 }).then(results => {
                  return resolve(results);
                }
                );
              });
            });
          }).then(getLast => {
            return app.service('blocks').find({
              query: {}
            }).then(blockFetched => {
              var newBlock = getFirstItem(blockFetched);
              if (newBlock == void 0) {
                return app.service('blocks').create({
                  hash: blockDetails.hash,
                  size: blockDetails.size,
                  height: blockDetails.height,
                  time: blockDetails.time,
                  version: blockDetails.version,
                  merkleroot: blockDetails.merkleroot,
                  tx: blockDetails.tx,
                  confirmations: blockDetails.confirmations,
                  nextblockhash: blockDetails.nextblockhash
                }).then(blockCreated => {
                  block = blockCreated.nextblockhash;
                  return block;
                });
              } else {
                return app.service('blocks').update(newBlock._id, {
                  $set: {
                    hash: blockDetails.hash,
                    size: blockDetails.size,
                    height: blockDetails.height,
                    time: blockDetails.time,
                    version: blockDetails.version,
                    merkleroot: blockDetails.merkleroot,
                    tx: blockDetails.tx,
                    confirmations: blockDetails.confirmations,
                    nextblockhash: blockDetails.nextblockhash
                  }
                }).then(blockUpdated => {
                  //  console.log("new hash   ", blockUpdated.hash)
                  block = blockUpdated.nextblockhash;
                  return block;
                });
              }
            });
          });
        if (block != void 0) {
          console.log('Now fetching this block ', block);
        } else {
          console.log('Latest block in blockchain ');
        }
      } catch (error) {
        console.log('Error within the loop', error);
      }

    }
  }

  var lastBlock = await app.service('blocks').find({
    query: {}
  }).then(blockFound => {
    var block = (getFirstItem(blockFound) !== void 0) ? getFirstItem(blockFound).hash : '000000000000077590e568390f4b83d3e7d09158dbc370ab2b447fa85df1436c';
    console.log(block, 'processing');
    return processingLatestBlock(block);
  }).catch(error => {
    console.log(error);
    return error;
  });

};
