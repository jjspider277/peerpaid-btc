var bitcoin = require('../../lib/bitcoin');
var debug = require('../../lib/debug');


var updateTimeout = 5000;
var minConfirmations = 2; // Minimum Confirmations for non pending status

var RECEIVE = 'receive';

var lastBlock = '';
var lastTxid = '';

var hourFee = '';
var halfHourFee = '';

module.exports = (transactionService, walletService) => {

  setTimeout(() => {
    refreshWallets().then(result => {
      setTimeout(() => {
        performUpdate();
      }, updateTimeout); // Update bitcoins every second.

    }).catch(err => {
      debug('RefreshWalletsFailed:', err);
    });
  }, 1000);

  // setInterval(() => {
  //   createTransaction();
  // }, 1000 * 60 * 10);

  // setInterval(() => {
  //   updateFees();
  // }, 60000);

  var currentlyUpdating = false;

  function performUpdate() {
    // debug('performUpdate');
    if (currentlyUpdating) {
      return;
    }
    currentlyUpdating = true;

    transactionService.find({
      paginate: false,
      query: {
        category: 'receive',
        $or: [
          { isQueue: true },
          { isPending: true },
          { isProcessed: false }
        ],
        $sort: {
          createdAt: -1
        }
      }
    }).then(results => {
      var promises = results.map((transaction) => {
        return new Promise((resolve, reject) => {
          function callGetRawTransaction(callCount) {
            bitcoin.command('getRawTransaction', transaction.txid, true).then((result) => {
              transaction = JSON.parse(JSON.stringify(transaction));
              if (transaction.confirmations !== result.confirmations || result.confirmations >= minConfirmations) {
                var tx = Object.assign({}, transaction, result);
                if (tx.confirmations >= minConfirmations) {
                  tx.isQueue = false;
                  tx.isPending = false;
                  if (tx.blocktime) {
                    tx.blocktimeDate = tx.blocktime + '000';
                  }

                  transactionService.update(tx._id, tx).then((result) => {
                    resolve();
                  }).catch(error => {
                    debug('Error updating transaction:', error);
                    resolve();
                  });
                } else if (!tx.confirmations) {
                  resolve();
                } else {
                  // update with latest data, and new confidence, then resolve
                  debug('processing transactions from queue');
                  tx.isQueue = false;
                  tx.isPending = true;
                  transactionService.update(tx._id, tx).then((updatedTransaction) => {
                    resolve();
                  }).catch(err => {
                    debug('Error updating pending transaction :', err);
                    resolve();
                  });
                }
              } else {
                resolve();
              }
            }).catch(error => {
              debug('getRawTransaction MyErr:', callCount, transaction.txid, error);
              callGetRawTransaction(callCount + 1);
            });
          }
          callGetRawTransaction(0);
        });
      });
      promises.push(Promise.resolve());

      Promise.all(promises).then(result => {
        enumLastBlock().then(result => {
          currentlyUpdating = false;
        }).catch(err => {
          currentlyUpdating = false;
          debug('WTF:', err);
        });
      }).catch(error => {
        currentlyUpdating = false;
        debug('Promise All + Enum Last Block MyErr:', error);
      });

    }).catch(err => {
      currentlyUpdating = false;
      debug('WTF::', err);
    });
  }

 
};