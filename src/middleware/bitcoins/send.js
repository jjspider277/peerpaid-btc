var bitcore = require('bitcore-lib');
var Decimal = require('decimal.js');

var io = require('socket.io-client');
const getFirstItem = require('../../lib/common').getFirstItem;

var Insight = require('./Insight');

module.exports = async (app, request) => {
  var insightServer = app.get('insightServer');

  return new Promise((resolve,reject)=>{
    
  })
  async function main() {
    var insight = new Insight(insightServer.url, insightServer.room);


    console.log("Sending bitcoins ");
    var value = new Buffer("t4estingsdf sdf sdf sdf ");
    var hash = bitcore.crypto.Hash.sha256(value);
    var bn = bitcore.crypto.BN.fromBuffer(value)
    var address = bitcore.PrivateKey(bn, 'testnet').toAddress();

    // address = 'mgGz16kHJn616eTmBZS8eFfxEM6TPdjmrH';

    address = 'mgQLUJ2u1SzEAfAV6Jh135yJRErSnGEBfX';

    // var privateKeySender = bitcoreLib.PrivateKey.fromString('cPrdxLwwqYZdVpdKVvRXfsdZNfpVgC41tfYUURjQ4rtELYdH2rDU');//mmk8ZdQdaXUdmKcWYEPDuHH59ceZQsnTmS
    // var privateKeyReceiver = bitcoreLib.PrivateKey.fromString('cVG6tmAWNPBwytSEpGEXzPVTenmQ8gHQBGKppGbEJWB5mgBJW1nD');//mo48SgAf2r1qiMpJR2iK5tAcvoCXz22PmW

    // var SendAddress = privateKeySender.toAddress();
    // var ReceiveAddress = privateKeyReceiver.toAddress();

    // // //var hashBuffer = bitcoreLib.crypto.Hash.sha256(new Buffer('blah'));

    var unspentPromise = new Promise((resolve, reject) => {
      insight.getUnspentUtxos(address, function (err, utxos) {
        if (err) {
          console.log("Error:", err);
          resolve(void 0);
        }
        else {
          console.log(utxos)
          resolve(utxos);
        }
      })
    })

    var unspent = await unspentPromise.catch(error => {
      console.log("unspentError:", error);
    });

    console.log("unspent:", unspent)
    if (!unspent || unspent.length === 0) {
      console.log("Nothing to Send");
      return;
    }
    var txout = unspent.splice(0, 100)


    var bitcoins = txout.reduce((accumulator, value) => {
      var decimal = new Decimal(bitcore.Unit.fromSatoshis(value.satoshis).toBTC());
      return accumulator.add(decimal);
    }, new Decimal(0.0));



    console.log("txout:", bitcoins.toString());
    console.log("Amount of satoshis", bitcore.Unit.fromBTC(request.payload.amount).toSatoshis());
    var tx = new bitcore.Transaction();
    tx.from(txout);
    tx.to(request.payload.wallet, bitcore.Unit.fromBTC(request.payload.amount).toSatoshis());
    //tx.change(request.payload.wallet);

    var currencyAccountOrigin = app.service('currency-accounts').get(request.payload.transaction.currencyAccountOrigin, {}).
      then(currencyAccount => {
        console.log("currency Account Origin", currencyAccount);
        return currencyAccount.wallet
      }).then(walletOrigin => {
        return app.service('private-keys').find({
          query: {
            address: address
          }
        }).then(keysStored => {
          var keyStored = getFirstItem(keysStored);
          if (keyStored === void 0) {
            console.log("no private Key Found !!");
          } else {
            tx.sign(keyStored.privateKey);
            tx.serialize();
            insight.broadcast(tx, (err, transactionId) => {
              if (err) {
                console.log("Error Broadcasting:", err);
              }
              else {
                console.log("TransactionID:", transactionId);
              }
            })
          }
        }).catch(error => {
          console.log("error", error);
        })
      })



    // if (false) insight.getUnspentUtxos(SendAddress, function (err, utxos) {
    //   if (err) {
    //     console.log("Error:", err);
    //   }
    //   else {

    //     console.log("Unspent:", utxos);

    //     return;
    //     var bitcoins = utxos.reduce((accumulator, value) => {
    //       var decimal = new Decimal(bitcoreLib.Unit.fromSatoshis(value.satoshis).toBTC());
    //       return accumulator.add(decimal);
    //     }, new Decimal(0.0));


    //     var tx = new bitcoreLib.Transaction();
    //     tx.from(utxos);

    //     // To send all bitcoins excluding fee, leave to blank and send to change address
    //     // tx.to(ReceiveAddress, bitcoreLib.Unit.fromBTC(bitcoins.toString()).toSatoshis());
    //     tx.change(ReceiveAddress);
    //     tx.sign(privateKeySender);


    //     console.log("Serialize:", tx.serialize());



    //     insight.broadcast(tx, (err, transactionId) => {
    //       if (err) {
    //         console.log("Error Broadcasting:", err);
    //       }
    //       else {
    //         console.log("TransactionID:", transactionId);
    //       }
    //     })

    //   }
    // })
  }


  main();

}