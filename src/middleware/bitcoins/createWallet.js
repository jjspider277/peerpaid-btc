var request = require('request');

var bitcoreLib = require('bitcore-lib');
var Decimal = require('decimal.js');
var io = require('socket.io-client');
var Insight = require('./insight');

module.exports = async (app) => {

  var insightServer = app.get('insightServer');
  var insight = new Insight(insightServer.url, insightServer.room);

  var privateKey = bitcoreLib.PrivateKey(insightServer.room);
  var newAddress = privateKey.toAddress()

  return app.service('private-keys').create({
    address: privateKey.toAddress(),
    privateKey: privateKey.toString()
  }).then(newPrivateKey => {
    return app.service('wallets').create({
      address: newPrivateKey.address,
      amount: 0,
    }).then(walletCreated => {
      return app.service('currency-accounts').create({
        accountId: walletCreated.address,
        currencyType: 'BTC'
      }).then(currencyAccount => {
        return walletCreated.address;
      })
    })
  })
}
