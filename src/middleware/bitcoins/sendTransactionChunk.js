
const Decimal = require('decimal.js');
var Promise = require("bluebird");

const getFirstItem = require('../../lib/common').getFirstItem;
var Insight = require('./insight');

var bitcore = require('bitcore-lib');
const slackClient = require('../../lib/slackClient')

const allocateInternalWallets = require('./allocateInternalWallets')

module.exports = (app) => {

  return new Promise((resolve, reject) => {
    const bitcoinConfiguration = app.get('bitcoin');

    const insightServer = app.get('insightServer');
    const insight = new Insight(insightServer.url, insightServer.room);
    var tx = new bitcore.Transaction();

    try {
      if (isNaN(bitcoinConfiguration.sendChunkEachSeconds) || bitcoinConfiguration.sendChunkEachSeconds <= 0) {
        slackClient(app, { message: 'sendChunkEachSeconds is an invalid number. Please check if the parameter bitcoinConfiguration.sendChunkEachSeconds exists and not null' });
        return reject(new Error('sendChunkEachSeconds is an invalid number. Please check if the parameter bitcoinCOnfiguration.sendChunkEachSeconds exists and not null'));
      } else {
        let pendingTransactions = app.service('internal-transaction').find({
          paginate: false,
          query: {
            type: 'SEND_BITCOINS',
            status: 'PENDING',
            currencyAccountDestination: { $exists: false },
          }
        }).then(pendingTransactions => {
          // console.log(pendingTransactions.length, ' pending transactions in total');
          if (pendingTransactions.length == 0) {
            throw new Error("No transactions found at this time")
          } else {
            var data = {
              amount: new Decimal(0),
              pk: [],
              transactions: []
            };
            return Promise.map(pendingTransactions, function (transaction) {
              return app.service('currency-accounts').get(transaction.currencyAccountOrigin, {}).then(currencyAccountFound => {
                return app.service('private-keys').find({
                  query: {
                    address: currencyAccountFound.accountId
                  }
                }).then(pKsFound => {
                  let pKFound = getFirstItem(pKsFound);
                  if (pKFound === void 0) {
                    return resolve("Sender address not valid ");
                  } else {
                    data.pk = [].concat(data.pk, pKFound.privateKey);
                    data.amount = data.amount.add(transaction.amount);
                    data.transactions = [].concat(data.transactions, { id: transaction._id, amount: transaction.amount, addressTo: transaction.details.wallet, walletFrom: currencyAccountFound.accountId });
                    return currencyAccountFound.accountId;
                  }
                })
              })
            }, { concurrency: 50 }).then(senderAddress => {
              data.amount = data.amount.toNumber();
              return data;
            })
          }
        }).then(data => {
          if (data == void 0) {
            return reject(new Error("No pending transactions"));
          }
          console.log("Data Amount for allocate ", data);
          return allocateInternalWallets(app, data.amount).then(values => {
            // console.log(values);
            return { internalWallets: values, data: data };
          });

        }).then(dataArray => {
          // console.log("WTF::: ", JSON.stringify(dataArray, null, 2));
          // console.log("PK  ", dataArray.internalWallets.pk)
          //  console.log("UTXO  ", dataArray.internalWallets.utxos)

          var tx = new bitcore.Transaction();
          dataArray.data.transactions.forEach(transaction => {
            console.log("address to send", transaction.addressTo);
            console.log("amount to send", transaction.amount);
            tx.to(transaction.addressTo, bitcore.Unit.fromBTC(transaction.amount).toSatoshis());
          });
          // var privateKey = new bitcore.PrivateKey("fc0c01a090e40a61c31bff8aee161c7df41ca253367a9cadb5bc6a0e1efa97f5", 'testnet');
          // var changeAddress = privateKey.toAddress();
          // console.log("PK Address", changeAddress);
          // console.log("Private Key ", privateKey);
          // console.log("utxos ", dataArray.internalWallets);
          // const changeAddress = dataArray.internalWallets.wallets.pop();
          // console.log(internalWalletChange);

          var internalWalletChange = Object.assign({}, dataArray.internalWallets.wallets[0]);

          //tx.serialize({ disableIsFullySigned: true });
          //var changeOutputSatoshis = tx.getChangeOutput().toObject().satoshis;
          //console.log(changeOutputSatoshis);
          //if amount of satoshis divided by amount of wallets under threshold
          //pick a new internal wallet;

          tx.from(dataArray.internalWallets.utxos);
          tx.change(internalWalletChange.address);
          const fee = tx.getFee();
          tx.fee(fee);
          tx.sign(dataArray.internalWallets.pk);

          if (tx.isFullySigned()) {
            tx.serialize();

            console.log("Fully serialized!");
            return new Promise((resolve, reject) => {
              insight.broadcast(tx, (err, transactionId) => {
                if (err) {
                  //console.log("Error Broadcasting:", err);
                  slackClient(app, { message: "Error when broadcasting a transaction: " + JSON.stringify(err, null, '\t') });
                  return reject(err);
                }
                else {
                  console.log("TransactionID:", transactionId);
                  slackClient(app, { message: "A new chunk of outgoing transaction was sent: " + JSON.stringify(tx.hash, null, '\t') });
                  return resolve(transactionId);
                }
              })
            }).then(blockChainTransaction => {

              //Uncomment if you want more details

              // var message = {
              //   txid: blockChainTransaction,
              //   UTXOs: dataArray.internalWallets.utxos,
              //   ChangeAddress: internalWalletChange.address,
              //   additionalWalletsUsed: dataArray.internalWallets.length,
              //   fee: fee,
              //   PK: dataArray.internalWallets.pk.length
              // }

              // var txs = [];
              // dataArray.data.transactions.forEach(transaction => {
              //   console.log("transaction amount", transaction);
              //   txs = [].concat({
              //     address: transaction.addressTo,
              //     amount: bitcore.Unit.fromSatoshis(transaction.amount).toBTC()
              //   })
              // });

              // message.txs = txs;
              //slackClient(app, { message: "A new chunk of transactions has been sent out!! : " + JSON.stringify(message, null, '\t') });

              return Promise.map(dataArray.data.transactions, function (transaction) {
                return app.service('internal-transaction').update(transaction.id, {
                  $set: {
                    blockchainTransaction: blockChainTransaction,
                    status: 'SUCCESS'
                  }
                }).then(transactionUpdated => {
                  return transactionUpdated
                })
              }, { concurrency: 50 }).then(transactions => {
                console.log("All transactions were updated", transactions);
                return Promise.map(dataArray.internalWallets.wallets, function (wallet) {
                  return app.service('internal-wallets').update(wallet._id, {
                    $set: {
                      updatedAt: new Date()
                    }
                  }).then(transactionUpdated => {
                    return transactionUpdated
                  })
                }, { concurrency: 50 }).then(internalWallets => {
                  console.log("All internal wallet were updated", internalWallets);
                  return transactions;
                });
              })
            })
          } else {
            var message = {
              UTXOs: dataArray.internalWallets.utxos,
              ChangeAddress: internalWalletChange.address,
              fee: fee,
              PK: dataArray.internalWallets.pk.length
            }
            slackClient(app, { message: "Error signing the transaction: " + JSON.stringify(message, null, '\t') });
            return resolve();
          }
        }).catch(error => {
          return reject(error)
        })
      }
    } catch (error) {
      return reject(error);
    }
  })
}
