var debug = require('../../lib/debug');
const getFirstItem = require('../../lib/common').getFirstItem;
const Decimal = require('decimal.js');
const minConfirmations = 2;

const Insight = require('./Insight');
debug = () => { };

var processingTransaction = false;
var transactionQueue = [];


const insight = new Insight('http://peerpaid-bitcore-dev:3001', 'testnet');


module.exports = (app) => {
  var currencyAccountService = app.service('currency-accounts');
  var transactionService = app.service('bitcoin-transactions');
  var walletService = app.service('wallets');
  var internalTransactionService = app.service('internal-transaction');

  transactionService.on('created', (transaction) => {
    tryProcessTransaction(transaction);
    createInternalTransaction(transaction);

  });
  transactionService.on('updated', (transaction) => {
    tryProcessTransaction(transaction);
    updateInternalTransaction(transaction);
  });


  walletService.on('created', (wallet) => {
    currencyAccountService.find({
      query: {
        accountId: wallet.address
      }
    }).then((currencyAccountData) => {
      let currencyAccount = getFirstItem(currencyAccountData);
      if (currencyAccount) {
        // Do Nothing?
      } else {
        currencyAccountService.create({
          accountId: wallet.address,
          currencyType: 'BTC'
        }).then((currencyAccount) => {
        }).catch((error) => {
        });
      }
    }).catch(error => {
    });
  });

  var updateInternalTransaction = (transaction) => {
    return new Promise((resolve, reject) => {
      if (transaction.confirmations < 10) {
        return updateTransactionProcessing(transaction, false);
      }
      return resolve("")
    }).catch(error => {
      debug("Error creating internal transaction", error);
    })
  }

  var createInternalTransaction = (transaction) => {
    return new Promise((resolve, reject) => {
      return currencyAccountService.find({
        query: {
          accountId: transaction.address
        }
      }).then((currencyAccount) => {
        currencyAccount = getFirstItem(currencyAccount);
        var amount = new Decimal(currencyAccount.amount);
        var tentativeAmount = new Decimal(currencyAccount.tentativeAmount);
        var transactionAmount = new Decimal(transaction.amount);
        return internalTransactionService.create({
          currencyAccountDestination: currencyAccount._id,
          amount: transactionAmount,
          currency: 'BTC',
          status: 'PENDING',
          type: 'RECEIVE_BITCOINS',
          owner: currencyAccount.owner,
          details: {
            transactionType: 'external',
            bitcoinTransaction: transaction._id
          }
        }).then(internalTransactionCreated => {
          tentativeAmount = tentativeAmount.add(transactionAmount);
          let txidsTentative = [].concat(currencyAccount.txidsTentative, [internalTransactionCreated._id]);
          // let txids = [].concat(currencyAccount.txids, []);

          currencyAccountService.update(currencyAccount._id, {
            $set: {
              amount,
              tentativeAmount,
              txidsTentative,
              updatedAt: Date.now()
            }
          }).then((currencyAccountUpdate) => {
            updateTransactionProcessing(transaction, false).then(resolve, reject);
          }).catch(error => {
            debug("Error updating currency Account", error)
          });
        }).catch(error => {
          debug("error ", error)
        })
      }).catch(error => {
        debug("Error creating internal transaction", error);
      })
    });
  }

  var tryProcessTransaction = (transaction) => {

    if (transaction.category === 'receive') {
      if (processingTransaction) {
        if (transactionQueue.indexOf(transaction._id.toString()) < 0) {
          transactionQueue.push(transaction._id.toString());
        }
      }
      else {
        processingTransaction = true;
        setTimeout(() => {
          processTransaction(transaction).then((result) => {
            processTransactionEnd();
          }).catch((error) => {
            debug("error in tryProcces Transaction", error)
            processTransactionEnd();
          });
        });
      }
    }
    else if (transaction.category !== 'send') {
      // Do Nothing
    }

  };

  var processTransaction = (transaction) => {

    return new Promise((resolve, reject) => {
      currencyAccountService.find({
        query: {
          accountId: transaction.address
        }
      }).then((currencyAccountData) => {
        var currencyAccount = getFirstItem(currencyAccountData);
        if (currencyAccount) {
          processCurrencyTransaction(currencyAccount, transaction).then(resolve, reject);
        } else {
          currencyAccountService.create({
            accountId: transaction.address,
            currencyType: 'BTC'
          }).then((currencyAccount) => {
            processCurrencyTransaction(currencyAccount, transaction).then(resolve, reject);
          }).catch(reject);
        }
      }).catch(reject);
    });

  };

  var processTransactionEnd = () => {

    var transactionEndPromise = new Promise((resolve, reject) => {
      if (transactionQueue.length > 0) {

        var transactionId = transactionQueue.splice(0, 1)[0];

        transactionService.get(transactionId).then((result) => {
          var transaction = getFirstItem(result);
          if (transaction) {
            reject();
            processTransaction(transaction);
          }
          else {
            reject();
            processTransaction(transaction).then((result) => {
              processTransactionEnd();
            }).catch((error) => {
              processTransactionEnd();
            });
          }

        }).catch(processTransactionEnd);
      }
      else {
        resolve();
      }
    });
    transactionEndPromise.then((result) => {
      processingTransaction = false;

    }).catch((error) => {
      setTimeout(() => { processTransactionEnd(); }, 1);
    });
  };

  var processCurrencyTransaction = (currencyAccount, transaction) => {

    return new Promise((resolve, reject) => {
      if ((currencyAccount.txidsTentative && currencyAccount.txidsTentative.indexOf(transaction.txid) >= 0)) {
        return processCurrencyTransactionAdd(currencyAccount, transaction);
      }
      else if (!currencyAccount.txids || currencyAccount.txids.indexOf(transaction.txid) < 0) {
        return processCurrencyTransactionAdd(currencyAccount, transaction);
      }
      else {
        resolve();
      }
    });
  };

  var processCurrencyTransactionAdd = (currencyAccount, transaction) => {
    return new Promise((resolve, reject) => {
      return internalTransactionService.find({
        query: {
          "details.bitcoinTransaction": transaction._id
        }
      }).then(internalTransactions => {

        return getFirstItem(internalTransactions);
      }).then(internalTransaction => {
        if (internalTransaction == void 0) {
          //console.log("no internal transaction found !!");
          return resolve();
        } else {
          //console.log("internal transaction found !", internalTransaction)
          var transactionAmount = new Decimal(transaction.amount);
          var amount = new Decimal(currencyAccount.amount);
          var tentativeAmount = new Decimal(currencyAccount.tentativeAmount);

          if (transaction.confirmations >= minConfirmations) {
            amount = amount.add(transactionAmount);
            let txids = [].concat(currencyAccount.txids, [transaction._id]);
            let txidsTentative = [].concat(currencyAccount.txidsTentative);


            currencyAccountService.update(currencyAccount._id, {
              $set: {
                amount,
                tentativeAmount,
                txids,
                txidsTentative,
                updatedAt: Date.now()
              }
            }).then((currencyAccountUpdate) => {

              updateTransactionProcessing(transaction, true).then(resolve, reject);
            }).catch(error => {
              console.log(error);
              return reject("Error");
            });
          }
        }
      });
    });
  };


  var removeTentativeIndex = (array, element) => {
    return array.filter(e => e != element);
  }


  var updateTransactionProcessing = (transaction, isProcessed) => {

    return new Promise((resolve, reject) => {
      if (transaction.confirmations == null) {
        console.log("transaction with confirmation not defined", transaction._id);
        return resolve();
      }
      if (transaction.confirmations >= minConfirmations) {
        internalTransactionService.find({
          query: {
            status: "PENDING",
            "details.bitcoinTransaction": transaction._id
          }
        }).then(internalTransactions => {
          return getFirstItem(internalTransactions);
        }).then(internalTransaction => {
          if (internalTransaction == void 0) {
            return resolve();
          } else {
            internalTransactionService.update(internalTransaction._id, {
              $set: {
                status: 'SUCCESS'
              }
            }).then(internalTransactionCompleted => {
              if (internalTransactionCompleted == void 0) {
                return resolve();
              }
              return currencyAccountService.get(internalTransactionCompleted.currencyAccountDestination).then(currencyAccount => {
                var amount = new Decimal(currencyAccount.amount);
                var tentativeAmount = new Decimal(currencyAccount.tentativeAmount);
                var txidsTentative = removeTentativeIndex(currencyAccount.txidsTentative, internalTransactionCompleted._id);
                let txids = [].concat(currencyAccount.txids, [internalTransactionCompleted._id]);

                amount = amount.add(internalTransactionCompleted.amount);
                tentativeAmount = tentativeAmount.minus(internalTransactionCompleted.amount);

                return currencyAccountService.update(currencyAccount._id, {
                  $set: {
                    amount: amount,
                    tentativeAmount: tentativeAmount,
                    txidsTentative: txidsTentative,
                    txids: txids
                  }
                }).then(currencyAccountUpdated => {
                  return transactionService.update(transaction._id, {
                    $set: {
                      isTentativeProcessed: !isProcessed,
                      isProcessed: isProcessed
                    }
                  }).then(resolve, reject);
                })

              })

            })
          }

        })

      } else {
        return resolve();
      }
    });
  };
};
