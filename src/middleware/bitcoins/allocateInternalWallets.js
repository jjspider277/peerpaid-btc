const Decimal = require('decimal.js');
const Promise = require("bluebird");

const slackClient = require('../../lib/slackClient')

const unspentTXOS = require('./unspentTXOS')


module.exports = (app, amount) => {
  var data = {
    pk: [],
    utxos: [],
    wallets: []
  };

  return new Promise((resolve, reject) => {
    return app.service('internal-wallets').find({
      // paginate: false,
      query: {
        $sort: {
          updatedAt: 1
        }
      }
    }).then(selectedWallets => {
      var unallocatedWallets = Object.assign([], selectedWallets.data);
      return Promise.map(unallocatedWallets, function (selectedWallet) {
        return unspentTXOS(app, selectedWallet.address).then(internalWalletUTXOs => {
          let calculatedAmount = internalWalletUTXOs.reduce((accumulator, value) => {
            var decimal = new Decimal(value.satoshis);
            return accumulator.add(decimal);
          }, new Decimal(0.0)).toNumber();
          return { utxos: internalWalletUTXOs, pk: selectedWallet.privateKey, amount: calculatedAmount, id: selectedWallet._id, address: selectedWallet.address }
        }).catch(error => {
          return reject(error);
        })
      })
    }).then(transactions => {
      let internalAmount = 0;
      return new Promise((resolve1, reject1) => {
        transactions.forEach(element => {
          if (internalAmount > amount) {
            // console.log("Fully resolved !!!!!!!!!!! ", element);
            return resolve1(data);
          } else {
            // console.log("internal amount", element);
            internalAmount = new Decimal(internalAmount).add(element.amount).toNumber();
            data.pk = [].concat(data.pk, element.pk);
            data.utxos = [].concat(data.utxos, element.utxos);
            data.wallets = [].concat(data.wallets, { _id: element.id, amount: element.amount, address: element.address });
            // console.log("We have enough btc to send", data);
          }
        });
      })
    }).then(finalResolve => {
      //console.log("We can create the transaction with this array ", JSON.stringify(finalResolve, null, 2));
      return resolve(finalResolve)
    }).catch(error => {
      return reject(error);
    })
  })

}