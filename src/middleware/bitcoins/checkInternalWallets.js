
const Decimal = require('decimal.js');
var Promise = require("bluebird");

const getFirstItem = require('../../lib/common').getFirstItem;
var Insight = require('./insight');

var bitcore = require('bitcore-lib');
const slackClient = require('../../lib/slackClient')

module.exports = (app) => {

  return new Promise((resolve, reject) => {
    const bitcoinConfiguration = app.get('bitcoin');


    var insightServer = app.get('insightServer');
    var insight = new Insight(insightServer.url, insightServer.room);

    try {
      if (isNaN(bitcoinConfiguration.internalWalletsCounter) || bitcoinConfiguration.internalWalletsCounter <= 0) {
        slackClient(app, { message: 'Internal Wallet counter is an invalid number. Please check if the parameter bitcoinConfiguration.internalWalletsCounter exists and not null' });
        return reject('Internal Wallet counter is an invalid number. Please check if the parameter bitcoinCOnfiguration.internalWalletsCounter exists and not null');
      } else {
        var internalWallets = app.service("internal-wallets").find({}).then(internalWalletsFound => {
          console.log(internalWalletsFound.total, 'internal wallets in total');
          const amountToCreate = new Decimal(bitcoinConfiguration.internalWalletsCounter).minus(internalWalletsFound.total);
          if (amountToCreate > 0) {
            var message = "We need to create " + amountToCreate.toNumber() + " more wallets ";
            slackClient(app, { message: message });

            var createArray = Array.from({ length: amountToCreate.toNumber() }).map((x, i) => { i });
            return Promise.map(createArray, function (iterator) {
              var privateKey = bitcore.PrivateKey(insightServer.room);
              var newAddress = privateKey.toAddress()
              return app.service('internal-wallets').create({
                address: privateKey.toAddress(),
                privateKey: privateKey.toString()
              }).then(internalWalletCreated => {
                var message = "An internal wallet with address " + internalWalletCreated.address + " was created. ";
                slackClient(app, { message: message });
                return internalWalletCreated;
              })

            }).then(allGood => {
              console.log("All internal wallets available")
              return resolve();
            })
          } else {
            console.log("We have all wallets created");
            return resolve();
          }
        })
      }
    } catch (error) {
      console.log(error);
      return reject(error);
    }
  })
}
