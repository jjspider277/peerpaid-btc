const bitcore = require('bitcore-lib');
const Insight = require('./Insight');
const axios = require('axios');
const Promise = require('bluebird');
const slackClient = require('../../lib/slackClient');

module.exports = async (app, address, confirmations = 2) => {
  const insightServer = app.get('insightServer');
  const insight = new Insight(insightServer.url, insightServer.room);

  return new Promise((resolve, reject) => {
    insight.getUnspentUtxos(address, function (err, utxos) {
      if (err) {  
        console.log('Error: ', err);
        return reject(err);
      } else {
        if (utxos.length === 0) {

          return reject(new Error('No unspent transactions found for this wallet: ' + address));
        } else {
          var messageArray = {
            address,
            utxos
          };
         // console.log('unspent transaction found for this address' + JSON.stringify(messageArray, null, '\t'), );
          var filteredUTXOS = [];

          Promise.map(utxos, function (tx) {
            var utxo = JSON.stringify(tx);
            utxo = JSON.parse(utxo);
            if (tx == void 0) {
              return [];
            } else {
              return axios.get(insightServer.url + '/insight-api/tx/' + utxo.txid, {})
                .then(response => {
                  if (response.data.confirmations >= confirmations) {
                    return tx;
                  }
                });
            }
          }).then(filteredUTXOS => {
           // console.log("Filtered UTXOS", filteredUTXOS)
            return resolve(filteredUTXOS);
          });
        }
      }
    });
  }).catch(error => {
    return [];
    //slackClient(app, { message: "Error getting UTXOS: " + JSON.stringify({ error: error }, null, '\t') });
  });
};