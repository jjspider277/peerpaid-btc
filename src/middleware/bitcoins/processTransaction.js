var debug = require('../../lib/debug');
const getFirstItem = require('../../lib/common').getFirstItem;
const Decimal = require('decimal.js');

debug = () => { };

module.exports = (app, tx, parentTransaction) => {

  var currencyAccountService = app.service('currency-accounts');
  var transactionService = app.service('internal-transaction');
  var walletService = app.service('wallets');

  const minConfirmations = app.get('bitcoin').minConfirmations;
  if (isNaN(minConfirmations) || minConfirmations <= 0) {
    console.log('Minimum Confirmations is an invalid number');
    return;
  }

  return new Promise((resolve, reject) => {

    if (!tx.scriptPubKey || !tx.scriptPubKey.addresses || tx.scriptPubKey.addresses.length == 0) {
      return resolve("pt 19");
    }


    var address = tx.scriptPubKey.addresses.pop();
    //find wallet 
    if (address) {
      return walletService.find({
        query: {
          address: address,
        }
      }).then(wallet => {
        // console.log("wallet", wallet)
        var wallet = getFirstItem(wallet)
        if (wallet == void 0) {
          // console.log("skip transaction")
          return resolve("pt 34", parentTransaction)
        } else {
          console.log(' !!!!!!!!!!!!!!!!!!!  internal transaction found for wallet ', address, ' !!!!!!!!!!');
          return transactionService.find({
            query: {
              blockchainTransaction: parentTransaction.txid,
              //status: 'PENDING'
            }
          }).then(transactions => {
            let transaction = getFirstItem(transactions);
            if (transaction == void 0) {
              return currencyAccountService.find({
                query: {
                  accountId: address,
                  currencyType: 'BTC'
                }
              }).then(currencyAccountResults => {
                let currencyAccount = getFirstItem(currencyAccountResults)
                let status = (parentTransaction.confirmations < minConfirmations) ? 'PENDING' : 'SUCCESS'
                console.log(parentTransaction.txid);
                //check if the transaction id is sent to an internal wallet, 
                return transactionService.create({
                  blockchainTransaction: parentTransaction.txid,
                  walletDestination: address,
                  type: 'RECEIVE_BITCOINS',
                  amount: new Decimal(tx.value),
                  owner: currencyAccount.owner,
                  currency: 'BTC',
                  status: status,
                  details: {
                    confirmations: (parentTransaction.confirmations == null) ? 0 : parentTransaction.confirmations
                  }
                }).then(resolved => {
                  if (status === 'SUCCESS') {
                    return currencyAccountService.update(currencyAccount._id, {
                      $addToSet: {
                        txids: resolved._id
                      },
                      $pull: {
                        txidsTentative: resolved._id
                      },
                      $inc: { tentativeAmount: new Decimal(resolved.amount).negated(), amount: new Decimal(resolved.amount) }
                    }).then(currencyAccountUpdated => {
                      return resolve("pt 73", parentTransaction);
                    })
                  } else if (status === 'PENDING') {
                    return currencyAccountService.update(currencyAccount._id, {
                      $addToSet: {
                        txidsTentative: resolved._id
                      },
                      $inc: { tentativeAmount: new Decimal(resolved.amount) }
                    }).then(currencyAccountUpdated => {
                      return resolve("pt 73", parentTransaction);
                    })
                  }
                }).catch(error => {
                  return reject()
                  console.log("error creating transaction", error)
                })
              })

            } else {
              console.log("transaction transactions");
              return transactionService.update(transaction._id, {
                $set: {
                  "details.confirmations": (parentTransaction.confirmations == null) ? 0 : parentTransaction.confirmations,
                  blockchainTransaction: parentTransaction.txid,
                }
              }).then(transaction => {
                return resolve("pt 69", parentTransaction);
              }).catch(err => {
                reject(err);
                console.log("error updating transaction:", err);
              })
            }

          })

        }
        //return 
        //Exist transaction ??

      }).catch(error => {
        reject(error);
        // console.log("skip transaction", tx.txid)
      })
    }
    else {
      resolve("pt 88");
    }
  })
};
