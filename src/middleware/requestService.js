const bitcoin = require('../lib/bitcoin');
const debug = require('../lib/debug');
const jwtverify = require('../lib/jwtverify');

const config = require('config');

const SERVER_TITLE = config.get('serverTitle');

const ADMIN_SERVER = config.get('serverTitles.admin');
const BTC_SERVER = config.get('serverTitles.btc');
const DATA_SERVER = config.get('serverTitles.data');
const FIAT_SERVER = config.get('serverTitles.fiat');
const WEB_SERVER = config.get('serverTitles.web');

const { processSendBitcoins } = require('../hooks/processSendBitcoins');
const createWallet = require('./bitcoins/createWallet')

const send = require('./bitcoins/send')

module.exports = function (app) {

  function init(app) {
    var requestService = app.service('requests');
    var walletService = app.service('wallets');
    var internalLedgerService = app.service('internal-ledger');
    if (!requestService) {
      setTimeout(() => {
        init(app);
      }, 100);
    }
    else {

      function requestUpdated(request) {
        if (request.token === SERVER_TITLE && SERVER_TITLE === BTC_SERVER) {
          debug(request);
          switch (request.request) {
            case 'REQUEST_BITCOIN_WALLET':
            case 'GET_BITCOIN_WALLET':
              switch (request.stage) {
                case 'CREATE_WALLET':

                  if (!request.address) {
                    createWallet(app).then(address => {
                      console.log("Creating a brand new wallet");
                      request.stage = 'ATTACH_ADDRESS',
                        request.address = address,
                        request.token = DATA_SERVER,
                        request.updatedAt = Date.now();
                      requestService.update(request._id, {
                        $set: request
                      }).then((request) => {
                      }).catch(error => {
                      });
                    }).catch(error => {
                    });
                  }
                  break; // case request.stage
                default:
                  debug('Unimplemented Stage', request.stage);
                  break; // case request.stage
              }
              break; // case request.request
          }
        }
      }

      requestService.on('created', (request) => {
        requestUpdated(request);
      });
      requestService.on('updated', (request) => {
        requestUpdated(request);
      });
      requestService.on('patched', (request) => {
        requestUpdated(request);
      });

    }
  }
  init(app);
};