var bitcoins = require('./bitcoins');
var requestService = require('./requestService');
var liveUpdates = require('./liveUpdates');

module.exports = function () {
  // Add your custom middleware here. Remember, that
  // in Express the order matters
  const app = this; // eslint-disable-line no-unused-vars

  // dataServer(app);
  bitcoins(app);
  requestService(app);
  liveUpdates(app);
  //queue(app);
};
