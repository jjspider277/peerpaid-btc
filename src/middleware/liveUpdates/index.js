const bitcoin = require('../../lib/bitcoin');
const debug = require('../../lib/debug');
const jwtverify = require('../../lib/jwtverify');

const config = require('config');

const SERVER_TITLE = config.get('serverTitle');

const ADMIN_SERVER = config.get('serverTitles.admin');
const BTC_SERVER = config.get('serverTitles.btc');
const DATA_SERVER = config.get('serverTitles.data');
const FIAT_SERVER = config.get('serverTitles.fiat');
const WEB_SERVER = config.get('serverTitles.web');


module.exports = function (app) {

  function init(app) {
    var liveDataService = app.service('live-data');
    if (!liveDataService) {
      setTimeout(() => {
        init(app);
      }, 100);
    }
    else {



      if (config.has('bitcoinAverageAPI')) {
        const liveDataService = app.service('live-data');

        const ba = require('bitcoinaverage');


        const publicKey = config.get('bitcoinAverageAPI').publicKey;
        const secretKey = config.get('bitcoinAverageAPI').secretKey;

        var restClient = ba.restfulClient(publicKey, secretKey);
        var wsClient = ba.websocketClient(publicKey, secretKey);

        try {
          // Here we log the response received by https://apiv2.bitcoinaverage.com/indices/global/ticker/BTCUSD. For custom usage you just need to implement the Anonimous function and do something else instead of debug(response);.
          restClient.tickerGlobalPerSymbol('BTCUSD', function (response) {
            debug('BTCUSD:', response);
          });

          var lastLiveData = void 0;

          // Here we show an example how to connect to one of our websockets and get periodical update for the Global Price Index for 'BTCUSD'. You can use 'local' instead of 'global', or you can change the crypto-fiat pair to something else (example: ETHEUR), depending on your needs.
          var reconnect = () => {
            debug('Connecting to web ticker');
            wsClient.connectToTickerWebsocket('global', 'BTCUSD', function (response) {
              lastLiveData = { '_id': 'abababababababababababab', name: 'BTCUSD', data: response };
              liveDataService.update('abababababababababababab', lastLiveData).then(result => {
                if (result === void 0 || result === null )liveDataService.create(lastLiveData);
              }).catch(error => {
                debug(error);
                
                liveDataService.create(lastLiveData);
              });
            });
          };

          wsClient.onClose = () => {
            reconnect();
          };
          reconnect();


        }
        catch (e) {
          debug('Error With Api:', e);
        }
      }


    }
  }
  init(app);
};
