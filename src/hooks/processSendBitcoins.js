
//TODO: Send Bitcoins
// We need A Ledger ***
// check that reqPayload.walletTo is not an internal wallet.
//        Do we have a currencyAccount or wallet for this waletAddress.
//        ****Data Server should check this originally, and this should not get here,
//          However the check should be in place just in case.
// check that reqPayload.requestId was not already sent. ((( Internal Ledger )))
//        This system should keep an internal ledger for requests based on the decrypted payload
// check that reqPayload.orderId was not already fulfilled. ((( Internal Ledger )))
//        This system should keep on the same internal ledger the orderId with the requestId
// Crete new item as a ledger and add txid upon completion. ((( Adds To Internal Ledger )))
//        This is the ledger that will be created with requestId and orderId
// Return txid to data server for its accounting needs


// sendfrom "n3khMTb7hwrLsyTBzPJSG1Z9Nu9AkpGCs7" "toaddress" amount ( minconf "comment" "comment_to" )
// bitcoin.command("sendfrom", "n3khMTb7hwrLsyTBzPJSG1Z9Nu9AkpGCs7",reqPayload.jwt.walletTo, reqPayload.jwt.amount).then(result => {

var common = {};
const bitcoin = require('../lib/bitcoin');

const getFirstItem = require('../lib/common').getFirstItem;

const debug = require('../lib/debug');


common.processSendBitcoins = (app, request) => {
  debug('data inside process send bitcoins', request);

  var walletService = app.service('wallets');
  var internalLedgerService = app.service('internal-ledger');
  var requestService = app.service('request');


  return new Promise((resolve, reject) => {

    try {
      debug('request inside processSendBitcoins  ');
      var reqPayload = JSON.parse(request.payload.userPermission);
      debug('request inside processSendBitcoins  ', reqPayload.walletTo);

      return internalLedgerService.find({
        query: {
          walletTo: reqPayload.walletTo,
          amount: reqPayload.amount,
          transactionId: reqPayload.transactionId,
          status: 'SUCCESS'
        }
      }).then(internalLedgerTransaction => {
        return getFirstItem(internalLedgerTransaction);
      }).then(internalLedgerTransaction => {

        if (internalLedgerTransaction == void 0) {
          //If the transaction was not found then
          // we create  a new one 
          return internalLedgerService.create({
            walletTo: reqPayload.walletTo,
            amount: reqPayload.amount,
            currency: reqPayload.currency || 'BTC',
            transaction: reqPayload.transactionId,
            status: 'PENDING'
          }).then(internalLedgerTransaction => {
            debug('internal Ledger', internalLedgerTransaction);
            return internalLedgerTransaction;
          }).catch(error => {
            debug('Error creating the internalledger transaction', error);
            return reject(error);
          });
        } else {
          debug('The transaction is being processed');
          return reject(internalLedgerTransaction);
        }
      }).then(internalLedgerTransaction => {
        debug(internalLedgerTransaction);
        var data = {
          internalLedger: internalLedgerTransaction._id,
        };
        return walletService.find({
          query: {
            address: request.payload.wallet
          }
        }).then(wallets => {
          return getFirstItem(wallets);

        }).then(wallet => {
          debug('Wallet ???????? ', wallet);
          if (wallet === void 0) {
            return data;
          } else {
            return reject('This is a internal transaction, error in the workflow');
          }
        }).then(data => {
          debug('Attempting to sent external transaction', data);
          return bitcoin.command('sendtoaddress', reqPayload.walletTo, reqPayload.amount).then(result => {
            debug('sent to address ??', result);
            return app.service('internal-transaction').update(reqPayload.transactionId, {
              $set: {
                status: 'SUCCESS'
              }
            }).then(internalTransactionUpdated => {
              data.confirmation = result;
              data.description = 'The transaction was succesfully performed';
              return resolve(data);
            })
          }).catch(error => {
            debug('Why did this happen:', error);
            data.description = 'Error completing the transaction';
            return reject(data);
            // TODO: What to do.
          });

        }).catch(errorWallet => {
          debug('error within wallet', errorWallet);
        });

      }).catch(error => {
        debug('Error happens', error);
        return reject('The transaction can\'t be completed');

      });
    }
    catch (error) {
      debug(error);
    }
  });
};

module.exports = common;



// common.processSendBitcoins = (app, request) => {


// }



