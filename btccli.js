#!/usr/local/bin/node

var bitcoin = require('./src/lib/bitcoin');



var args = [];

for (i = 2; i < process.argv.length; i++) {
  try {
    args.push(JSON.parse(process.argv[i]));
  }
  catch (e){
    args.push(process.argv[i]);
  }
}



try {

  bitcoin.command.apply(bitcoin,args).then(result =>{
   if (args[0] === 'help'){
      console.log(result);
    }
    else {
      Object.keys(result).forEach((key) => {
        if (Array.isArray(result[key])){
          result[key + '[' + result[key].length + ']' ] = result[key];
          delete result[key];
        }
      }); 
      console.log("Result:", JSON.stringify(result, null, 2));
      
    }
  }).catch ( error=> {
    console.log("ror:", error);
  });
  
}
catch (e) {
 console.log("Err:",e);
}


// bitcoin.listReceivedByAddress(callback)
